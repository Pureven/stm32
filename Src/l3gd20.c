/**
  ******************************************************************************
  * File Name          : l3gd20.c
  * Description        : This file provides driver code for gyroscope
  *
  ******************************************************************************
*/

#include "l3gd20.h"
#include "stm32f3xx.h"
#include "spi.h"
#include "usart.h"


uint8_t l3gd20_read(uint8_t address)
{
    uint8_t tx_data[2] = {address | 0x80, 0x0};
    uint8_t rx_data[2];

    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);

    HAL_SPI_TransmitReceive(&hspi1, tx_data, rx_data, 2, 100);

    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);

    return rx_data[1];
}

void l3gd20_write(uint8_t address, uint8_t data)
{
    uint8_t tx_data[2] = {address | 0x00, data};

    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_RESET);

    HAL_SPI_Transmit(&hspi1, tx_data, 2, 100);

    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_3, GPIO_PIN_SET);
}

