#include "FreeRTOS.h"
#include "task.h"
#include "cmsis_os.h"
#include "gpio.h"
#include "main.h"

osThreadId defaultTaskHandle;
osThreadId ledTaskHandle;
osThreadId accelo;


void StartDefaultTask(void const * argument);
void StartLedTask(void const * argument);
void StartaccTask(void const * argument);
void MX_FREERTOS_Init(void); /* (MISRA C 2004 rule 8.1) */



void StartGyroCheckTask(void const * argument);

void MX_FREERTOS_Init(void) {
  osThreadDef(defaultTask, StartDefaultTask, osPriorityNormal, 0, 128);
  defaultTaskHandle = osThreadCreate(osThread(defaultTask), NULL);

  osThreadDef(ledTask, StartLedTask, osPriorityNormal, 0, 128);
  ledTaskHandle = osThreadCreate(osThread(ledTask), NULL);

  osThreadDef(accTask, StartaccTask, osPriorityNormal, 0, 256);
  accelo = osThreadCreate(osThread(accTask), NULL);
}


void StartDefaultTask(void const * argument)
{

  for(;;)
  {

    osDelay(100);

  }
}



void StartLedTask(void const * argument)
{

  for(;;)
  {
         allOn();
         printACC();
osDelay(100);

  }
}
void StartaccTask(void const * argument)
{

  for(;;)
  {
    printMagnets();
    PrintGyro();
    osDelay(100);
  }
}
//void StartallON(void const * argument)
//{
//    for(;;)
//    {

//    }
//}
