#include "main.h"
#include "stm32f3xx_hal.h"
#include "cmsis_os.h"
#include "i2c.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

#include "l3gd20.h"
#include "acc_mag.h"
#define LSM303_ACC_Z_ENABLE 0x07 // 0000 0100
#define LSM303_ACC_100HZ 0x50 //0101 0000
char kk []="Elo napisz cos\n\r";
char pp [10];
char buffor[10];
int armed=0;
int16_t ZaxisM=0, YaxisM=0, XaxisM=0,tmp=0;
uint8_t data=0;
uint8_t dataL=0;
int16_t sum=0;
int16_t Zaxis =0, Yaxis=0, Xaxis=0;
uint8_t Settings = LSM303_ACC_Z_ENABLE | LSM303_ACC_100HZ;
uint8_t Cra_mag = 0x00010000;
uint8_t Crb_mag = 0x01100000;
uint8_t Mr_reg = 0x00000000;
uint8_t ptxData = 0;
uint8_t prxData = 0;
uint8_t prxDataL =0;

void SystemClock_Config(void);
void MX_FREERTOS_Init(void);
int  abs(int n)
{
    if(n<0) return -n;
    else return n;
}
void changeLights()
{


    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_SET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_RESET);
    HAL_Delay(1000);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_RESET);
    HAL_Delay(1000);

}
void allOn()
{

    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_SET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_SET);

    HAL_Delay(200);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_8, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_12, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_9, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_13, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_10, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_14, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_11, GPIO_PIN_RESET);
    HAL_GPIO_WritePin(GPIOE, GPIO_PIN_15, GPIO_PIN_RESET);
    HAL_Delay(200);
}
void printACC()
{
    printf("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX\n");

    HAL_I2C_Mem_Read(&hi2c1,ACC_I2C_ADDRESS,OUT_Z_H_A , 1, &data,1,100);

    Zaxis = data<<8;
    data=0;
    Zaxis/=256;
    printf("Z axis : %d\n",Zaxis);
    if(abs(ZaxisM)<abs(Zaxis))
    {
        ZaxisM = Zaxis;

    }
      printf("Z AxisMAX: %d\n",ZaxisM);

    HAL_I2C_Mem_Read(&hi2c1,ACC_I2C_ADDRESS,OUT_Y_H_A , 1, &data,1,100);
    Yaxis = data <<8;
    data=0;
    Yaxis/=256;
    printf("Y axis : %d\n",Yaxis);
    if(abs(XaxisM)<abs(Xaxis))
    {
        XaxisM =Xaxis;

    }
      printf("X AxisMAX: %d\n",XaxisM);




    HAL_I2C_Mem_Read(&hi2c1,ACC_I2C_ADDRESS,OUT_X_H_A , 1, &data,1,100);
    Xaxis = data <<8;
    data=0;
    Xaxis/=256;
    printf("X axis : %d\n",Xaxis);
    if(abs(YaxisM)<abs(Yaxis))
    {
        YaxisM = Yaxis;

    }
      printf("Y AxisMAX: %d\n",YaxisM);

    HAL_Delay(1000);
}
void printMagnets()
{
    printf("XXXXXXXXMAGNETSXXXXXXXXXXXX\n\r");
     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_Y_H_M,1,&data,1,100);
     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_Y_L_M,1,&dataL,1,100);
     //data=data<<8;
     sum=data<<8|dataL;
     printf("Y Magnet: %d\n",sum);

     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_X_H_M,1,&data,1,100);
     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_X_L_M,1,&dataL,1,100);
     sum=data<<8|dataL;
     //data=data<<8;
     printf("X Magnet: %d\n",sum);

     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_Z_H_M, 1, &data,1,100);
     HAL_I2C_Mem_Read(&hi2c1, MAG_I2C_ADDRESS,OUT_Z_L_M,1,&dataL,1,100);
    // data=data<<8;
     sum=data<<8|dataL;
     printf("Z Magnet: %d\n",sum);
     osDelay(1000);
}
void PrintGyro()
{
    l3gd20_write(L3GD20_REG_CTRL_REG1,0x0F);


    sum=l3gd20_read(L3GD20_REG_OUT_X_H)<<8 | l3gd20_read(L3GD20_REG_OUT_X_L);
                 printf("Gyro X: %f\n",(float)sum*L3GD20_SENSITIVITY_250/1000);

    sum=l3gd20_read(L3GD20_REG_OUT_Y_H)<<8 | l3gd20_read(L3GD20_REG_OUT_Y_L);
                printf("Gyro Y: %f\n",(float)sum*L3GD20_SENSITIVITY_250/1000);

    sum=l3gd20_read(L3GD20_REG_OUT_Z_H)<<8 | l3gd20_read(L3GD20_REG_OUT_Z_L);
                 printf("Gyro Z: %f\n",(float)sum*L3GD20_SENSITIVITY_250/1000);
    osDelay(1000);
}

int main(void)
{
  HAL_Init();
  SystemClock_Config();

  MX_GPIO_Init();
  MX_I2C1_Init();
  MX_SPI1_Init();
  MX_USART1_UART_Init();

  MX_FREERTOS_Init();

//0x8c 0x20

    HAL_I2C_Mem_Write(&hi2c1, MAG_I2C_ADDRESS, CRA_REG_M, 1, &Cra_mag, 1, 100);
    HAL_I2C_Mem_Write(&hi2c1, MAG_I2C_ADDRESS, CRB_REG_M, 1, &Crb_mag, 1, 100);
    HAL_I2C_Mem_Write(&hi2c1, MAG_I2C_ADDRESS, MR_REG_M, 1, &Mr_reg, 1, 100);

  if(HAL_I2C_IsDeviceReady(&hi2c1, ACC_I2C_ADDRESS, 5, 100) == HAL_OK)
  {
      printf("Ready to use Accelerometr\n\r");
  }
  HAL_I2C_Mem_Write(&hi2c1, ACC_I2C_ADDRESS, CTRL_REG1_A, 1, &Settings, 1, 100);
  HAL_Delay(200);
  if(HAL_I2C_IsDeviceReady(&hi2c1, MAG_I2C_ADDRESS, 5, 100) == HAL_OK)
  {
      printf("Ready to use magnetometer\n\r");
  }
  if(HAL_SPI_TransmitReceive(&hspi1, L3GD20_REG_WHO_AM_I, &prxData, 5,100) == HAL_OK)
  {
      printf("Ready to use Gyroscope\n\r");
  }
  printACC();

  /* Start scheduler */
  osKernelStart();   /* Codde below this line will not be executed! */
  while (1)
  {

  }
}






/***Do not modify **************************************************************/

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{

  RCC_OscInitTypeDef RCC_OscInitStruct;
  RCC_ClkInitTypeDef RCC_ClkInitStruct;
  RCC_PeriphCLKInitTypeDef PeriphClkInit;

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSE;
  RCC_OscInitStruct.HSEState = RCC_HSE_BYPASS;
  RCC_OscInitStruct.HSEPredivValue = RCC_HSE_PREDIV_DIV1;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = 16;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSE;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL9;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Initializes the CPU, AHB and APB busses clocks 
    */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK2;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    _Error_Handler(__FILE__, __LINE__);
  }

    /**Configure the Systick interrupt time 
    */
  HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq()/1000);

    /**Configure the Systick 
    */
  HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

  /* SysTick_IRQn interrupt configuration */
  HAL_NVIC_SetPriority(SysTick_IRQn, 15, 0);
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  Period elapsed callback in non blocking mode
  * @note   This function is called  when TIM1 interrupt took place, inside
  * HAL_TIM_IRQHandler(). It makes a direct call to HAL_IncTick() to increment
  * a global variable "uwTick" used as application time base.
  * @param  htim : TIM handle
  * @retval None
  */
void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim)
{
  /* USER CODE BEGIN Callback 0 */

  /* USER CODE END Callback 0 */
  if (htim->Instance == TIM1) {
    HAL_IncTick();
  }
  /* USER CODE BEGIN Callback 1 */

  /* USER CODE END Callback 1 */
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  file: The file name as string.
  * @param  line: The line in file as a number.
  * @retval None
  */
void _Error_Handler(char *file, int line)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  while(1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
  * @}
  */

/**
  * @}
  */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
